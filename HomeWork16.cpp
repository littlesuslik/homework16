﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>                 
#include <ctime>  

int main()
{
    auto now = time(nullptr);
    const auto ltm = localtime(&now);
    const int N = 2;
    int array[N][N];

    for (auto i = 0; i < N; i++)
    {
        for (auto j = 0; j < N; j++)
        {
           array[i][j] = i + j;
        }
    }

    std::cout << "array: " << N << "x" << N << std::endl;
    for (auto i = 0; i < N; i++)
    {
        for (auto j = 0; j < N; j++)
        {
            if (i + j < 10)
                std::cout << " ";
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    static int number;
    for (auto i = 0; i < N; i++)
    {
        if (i == ltm->tm_mday % N)
            for (auto j = 0; j < N; j++)
            {
                number += array[i][j];
            }

        if (ltm->tm_mday % N == i)
            std::cout << "string: " << i << " number: " << number << " ";
    }
}
